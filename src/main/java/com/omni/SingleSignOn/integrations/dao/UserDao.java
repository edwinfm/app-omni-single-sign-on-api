package com.omni.SingleSignOn.integrations.dao;

import com.omni.SingleSignOn.commons.dto.ReqUserDto;
import com.omni.SingleSignOn.commons.exceptions.BusinessException;
import com.omni.SingleSignOn.commons.utils.ConnectionManagerUtil;
import com.omni.SingleSignOn.commons.utils.DbUtil;
import com.omni.SingleSignOn.integrations.entity.UserEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDao {

    private final @NonNull NamedParameterJdbcTemplate jdbcTemplate;
    private final @NonNull DbUtil dbUtil;

    public UserEntity getGetUserByUsername(String username)
            throws BusinessException {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("in_username", username);

            String sql = this.dbUtil.getGetUserByUsername();

            return this.jdbcTemplate.queryForObject(sql, params, new BeanPropertyRowMapper<>(UserEntity.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            throw new BusinessException("No se logro completar la consulta del usuario");
        } finally {
            ConnectionManagerUtil.closeJdbc(this.jdbcTemplate);
        }
    }

    public void createUser(ReqUserDto reqUserDto)
            throws BusinessException {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("in_full_name", reqUserDto.getNombreCompleto());
            params.addValue("in_address", reqUserDto.getDireccion());
            params.addValue("in_email", reqUserDto.getEmail());
            params.addValue("in_username", reqUserDto.getUsuario());
            params.addValue("in_password", reqUserDto.getContraseña());

            String sql = this.dbUtil.getCreateUser();

            this.jdbcTemplate.update(sql, params);

        } catch (DataAccessException e) {
            throw new BusinessException("No se logro crear el usuario");
        } finally {
            ConnectionManagerUtil.closeJdbc(this.jdbcTemplate);
        }
    }
}
