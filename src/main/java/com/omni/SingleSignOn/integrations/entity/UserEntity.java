package com.omni.SingleSignOn.integrations.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserEntity implements Serializable {
    private static final long serialVersionUID = -6524014532966250472L;
    private int usersId;
    private String fullName;
    private String address;
    private String email;
    private String username;
    private String password;
}
