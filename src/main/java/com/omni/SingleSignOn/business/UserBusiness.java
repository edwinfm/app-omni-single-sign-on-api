package com.omni.SingleSignOn.business;

import com.omni.SingleSignOn.commons.dto.ReqAuthUserDto;
import com.omni.SingleSignOn.commons.dto.ReqUserDto;
import com.omni.SingleSignOn.commons.dto.RespAuthUserDto;
import com.omni.SingleSignOn.commons.dto.RespResultDto;
import com.omni.SingleSignOn.commons.enums.ResponseEnum;
import com.omni.SingleSignOn.commons.exceptions.BusinessException;
import com.omni.SingleSignOn.commons.utils.EncodeUtil;
import com.omni.SingleSignOn.commons.utils.JwtUtil;
import com.omni.SingleSignOn.integrations.dao.UserDao;
import com.omni.SingleSignOn.integrations.entity.UserEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserBusiness {

    private final @NonNull JwtUtil jwtUtil;
    private final @NonNull EncodeUtil encodeUtil;
    private final @NonNull UserDao userDao;

    public RespAuthUserDto authUser(ReqAuthUserDto authUser) throws BusinessException {

        UserEntity userEntity = this.userDao.getGetUserByUsername(authUser.getUsername());

        if (ObjectUtils.isEmpty(userEntity))
            throw new BusinessException("No se encontró ningún usuario con los parámetros de entrada");

        if (!this.encodeUtil.comparePass(authUser.getPassword(), userEntity.getPassword()))
            throw new BusinessException("Usuario o contraseña incorrectas");

        Map<String, Object> claims = new HashMap<>();
        claims.put("name", userEntity.getFullName());
        String token = this.jwtUtil.getJWTToken(authUser.getUsername(), claims);

        RespAuthUserDto response = RespAuthUserDto.builder().build();
        response.setResultado(new ArrayList<>());
        response.setContenido(new ArrayList<>());

        response.getResultado().add(
                RespAuthUserDto.Result.builder()
                        .tipo(ResponseEnum.SUCCESS.getType())
                        .descripcion(ResponseEnum.SUCCESS.getMessage())
                        .codigo(ResponseEnum.SUCCESS.getCode())
                        .build()
        );

        response.getContenido().add(
                RespAuthUserDto.Content.builder()
                        .token(token)
                        .username(authUser.getUsername())
                        .build()
        );

        return response;
    }

    public RespResultDto createUser(ReqUserDto reqUserDto) throws BusinessException {

        UserEntity userEntity = this.userDao.getGetUserByUsername(reqUserDto.getUsuario());

        if (!ObjectUtils.isEmpty(userEntity))
            throw new BusinessException("El usuario no puede estar duplicado");

        reqUserDto.setContraseña(this.encodeUtil.encodePass(reqUserDto.getContraseña()));
        this.userDao.createUser(reqUserDto);

        RespAuthUserDto response = RespAuthUserDto.builder().build();
        response.setResultado(new ArrayList<>());

        response.getResultado().add(
                RespAuthUserDto.Result.builder()
                        .tipo(ResponseEnum.SUCCESS.getType())
                        .descripcion(ResponseEnum.SUCCESS.getMessage())
                        .codigo(ResponseEnum.SUCCESS.getCode())
                        .build()
        );

        return response;
    }

}
