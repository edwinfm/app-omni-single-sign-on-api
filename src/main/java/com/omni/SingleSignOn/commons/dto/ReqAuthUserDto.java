package com.omni.SingleSignOn.commons.dto;

import lombok.Data;

@Data
public class ReqAuthUserDto {

    private String username;
    private String password;
}
