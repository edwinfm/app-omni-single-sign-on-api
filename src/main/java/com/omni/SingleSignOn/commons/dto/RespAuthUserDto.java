package com.omni.SingleSignOn.commons.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class RespAuthUserDto extends RespResultDto implements Serializable {

    private static final long serialVersionUID = -6908174846833812158L;
    private List<Content> contenido;

    @Data
    @Builder
    public static class Content {
        private String username;
        private String token;
    }

    public static class RespAuthUserDtoBuilder extends RespResultDtoBuilder {
        RespAuthUserDtoBuilder() {
            super();
        }
    }
}
