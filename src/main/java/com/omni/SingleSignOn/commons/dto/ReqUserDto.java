package com.omni.SingleSignOn.commons.dto;

import lombok.Data;

@Data
public class ReqUserDto {
    private String nombreCompleto;
    private String direccion;
    private String email;
    private String usuario;
    private String contraseña;
}
