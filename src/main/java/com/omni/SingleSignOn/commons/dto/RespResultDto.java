package com.omni.SingleSignOn.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RespResultDto implements Serializable {

    private static final long serialVersionUID = 4417002353203359405L;
    private List<Result> resultado;

    @Data
    @Builder
    public static class Result {
        private String codigo;
        private String descripcion;
        private String tipo;
    }
}
