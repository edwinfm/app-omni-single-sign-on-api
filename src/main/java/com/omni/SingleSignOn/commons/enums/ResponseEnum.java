package com.omni.SingleSignOn.commons.enums;

import lombok.Getter;

@Getter
public enum ResponseEnum {

    SUCCESS("RP_200","Transacción Exitosa","INFO"),
    ERROR("RP_400","No se pudo completar la transaccion","ERROR");

    private String code;
    private String message;
    private String type;

    ResponseEnum(String code,String message,String type) {
        this.code = code;
        this.message = message;
        this.type = type;
    }
}
