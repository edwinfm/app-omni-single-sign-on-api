package com.omni.SingleSignOn.commons.exceptions;

import com.omni.SingleSignOn.commons.dto.RespResultDto;
import com.omni.SingleSignOn.commons.enums.ResponseEnum;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    private ResponseEntity<Object> getObjectResponseEntity(Exception ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        String message = ex.getLocalizedMessage();
        if (!StringUtils.hasLength(message))
            message = ResponseEnum.ERROR.getMessage();

        RespResultDto response = RespResultDto.builder().build();
        response.setResultado(new ArrayList<>());
        response.getResultado().add(RespResultDto.Result.builder()
                .tipo(ResponseEnum.ERROR.getType())
                .descripcion(message)
                .codigo(ResponseEnum.ERROR.getCode())
                .build());

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(
            Exception ex,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(
            BusinessException ex,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
            HttpMediaTypeNotAcceptableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return getObjectResponseEntity(ex);
    }

    @Override
    public ResponseEntity<Object> handleAsyncRequestTimeoutException(
            AsyncRequestTimeoutException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest webRequest) {
        return getObjectResponseEntity(ex);
    }
}
