package com.omni.SingleSignOn.commons.utils;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EncodeUtil {
    private final @NonNull PasswordEncoder passwordEncoder;

    public String encodePass(String value) {
        return this.passwordEncoder.encode(value);
    }

    public boolean comparePass(String passText,String passEncode) {
        return this.passwordEncoder.matches(passText, passEncode);
    }
}
