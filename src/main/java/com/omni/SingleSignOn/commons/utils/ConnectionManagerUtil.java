package com.omni.SingleSignOn.commons.utils;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class ConnectionManagerUtil {

    public static void closeJdbc(NamedParameterJdbcTemplate jdbcTemplate) {
        try {
            DataSourceUtils.getConnection(Objects.requireNonNull(jdbcTemplate.getJdbcTemplate().getDataSource())).close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionManagerUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
