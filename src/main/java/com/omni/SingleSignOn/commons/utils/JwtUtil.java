package com.omni.SingleSignOn.commons.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class JwtUtil {

    @Value("${jwt.secret-word}")
    private String jwtSecretWord;
    @Value("${jwt.time-expiration}")
    private String jwtTimeExpiration;
    @Value("${jwt.issuer}")
    private String issuer;
    @Value("${spring.application.name}")
    private String appName;

    public String getJWTToken(String username, Map<String, Object> claims) {

        int timeExp = Integer.parseInt(jwtTimeExpiration) * 60000;
        List<String> roles = Arrays.asList( "ROLE_ADMIN" );
        String token = Jwts.builder()
                .setId(appName)
                .setSubject(username)
                .addClaims(claims)
                .claim("authorities",roles)
                .signWith(SignatureAlgorithm.HS512, jwtSecretWord.getBytes())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setIssuer(issuer)
                .setExpiration(new Date(System.currentTimeMillis() + timeExp))
                .compact();

        return "Bearer " + token;
    }
}
