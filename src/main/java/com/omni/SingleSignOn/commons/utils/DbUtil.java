package com.omni.SingleSignOn.commons.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "bd.sp")
@EnableConfigurationProperties

@Data
public class DbUtil {

    private String getUserByUsername;
    private String createUser;
}
