package com.omni.SingleSignOn.apis;

import com.omni.SingleSignOn.business.UserBusiness;
import com.omni.SingleSignOn.commons.dto.ReqAuthUserDto;
import com.omni.SingleSignOn.commons.dto.ReqUserDto;
import com.omni.SingleSignOn.commons.dto.RespAuthUserDto;
import com.omni.SingleSignOn.commons.dto.RespResultDto;
import com.omni.SingleSignOn.commons.exceptions.BusinessException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserApi {

    private final @NonNull UserBusiness userBusiness;

    @PostMapping( path = "user/auth", produces = "application/json" )
    public ResponseEntity<RespAuthUserDto> authUser(@RequestBody ReqAuthUserDto reqAuthUserDto) throws BusinessException {
        return new ResponseEntity<>(this.userBusiness.authUser(reqAuthUserDto), HttpStatus.OK );
    }

    @PostMapping( path = "user", produces = "application/json" )
    public ResponseEntity<RespResultDto> createUser(@RequestBody ReqUserDto reqUserDto) throws BusinessException {
        return new ResponseEntity<>(this.userBusiness.createUser(reqUserDto), HttpStatus.OK );
    }
}
